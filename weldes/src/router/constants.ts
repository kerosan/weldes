export const ROUTES = {
  ROOT: '/',
  HOME: '/home',
  ABOUT: '/about',
  SERVICE: '/service',
  PRICE: '/price',
  OUR_WORK: '/our-work',
  ARTICLES: '/articles',
  CONTACT: '/contact'
}
