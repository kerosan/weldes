import React, { lazy, Suspense } from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { Route, Switch } from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress'

import { ROUTES } from '@app/router/constants'

const WithFallback = props => {
  const { lazy, ...extra } = props
  const Lazy = lazy
  return (
    <Suspense fallback={<CircularProgress />}>
      <Lazy {...extra} />
    </Suspense>
  )
}

const Main = props => (
  <WithFallback lazy={lazy(() => import('@containers/main'))} extra={props} />
)
const About = props => (
  <WithFallback
    lazy={lazy(() => import('@containers/articlesTextPage'))}
    extra={props}
  />
)
const Service = props => (
  <WithFallback
    lazy={lazy(() => import('@containers/serviced'))}
    extra={props}
  />
)
const Price = props => (
  <WithFallback
    lazy={lazy(() => import('@containers/articlesTextPage'))}
    extra={props}
  />
)
const OurWork = props => (
  <WithFallback
    lazy={lazy(() => import('@containers/ourWork'))}
    extra={props}
  />
)
const Articles = props => (
  <WithFallback
    lazy={lazy(() => import('@containers/articles'))}
    extra={props}
  />
)
const Contact = props => (
  <WithFallback
    lazy={lazy(() => import('@containers/contact'))}
    extra={props}
  />
)

const AppRouter = ({ history }) => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route exact={true} path={ROUTES.ROOT} render={Main} />
      <Route exact={true} path={ROUTES.ABOUT} render={About} />
      <Route exact={true} path={ROUTES.SERVICE} render={Service} />
      <Route exact={true} path={ROUTES.PRICE} render={Price} />
      <Route exact={true} path={ROUTES.OUR_WORK} render={OurWork} />
      <Route exact={true} path={ROUTES.ARTICLES} render={Articles} />
      <Route exact={true} path={ROUTES.CONTACT} render={Contact} />
    </Switch>
  </ConnectedRouter>
)

export default AppRouter
