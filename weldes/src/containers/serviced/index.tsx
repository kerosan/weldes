import { useStyles } from '@containers/contact/connectMui'
import { Grid } from '@material-ui/core'
import { Button } from '@common/button'
import { Icon } from '@common/icon/icon'
import UpMenu from '@components/topMenu'
import NavMenu from '@common/navMenu'
import { UnderNav } from '@common/underNawMenu'
import Img from '@assets/navMenu/service.png'
import Breadcrumbs from '@common/breadcrumbs'
import Footer from '@common/footer'
import React from 'react'
import ScrollToTop from 'react-scroll-up'
import MediaList from '@components/services'
import { compose } from 'redux'

const Servised = props => {
  const style = useStyles()
  return (
    <Grid className={style.containers}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <Grid container className={style.content}>
        <UpMenu />
        <NavMenu />
        <UnderNav label={'Услуги'} img={Img} />
        <Breadcrumbs />
        <MediaList className={style.content} />
        <Footer />
      </Grid>
    </Grid>
  )
}
export default compose()(Servised)
