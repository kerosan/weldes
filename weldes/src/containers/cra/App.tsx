import React, { useEffect, useState } from 'react'
import './App.css'
import axios from 'axios'

const App: React.FC = () => {
  const [data, setData] = useState()
  useEffect(() => {
    const fetch = async () => {
      const res = await axios.get('/api/get_posts')
      setData(res)
    }
    fetch()
  }, [])
  return (
    <div className="App">
      <header className="App-header">
        <div
          style={{ width: 300, margin: '0 auto' }}
          dangerouslySetInnerHTML={{
            __html: data && data.data.posts[0].content
          }}
        />
        <div>status: {data && data.data.status}</div>
      </header>
    </div>
  )
}

export default App
