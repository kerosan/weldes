import makeStyles from '@material-ui/core/styles/makeStyles'

import createStyles from '@material-ui/core/styles/createStyles'
import { Color, fontFamily } from '@app/theme'
import Img from '@assets/navMenu/main.png'

export const useStyles = makeStyles(() =>
  createStyles({
    container: {
      backgroundImage: (props: any) => `url("${Img}")`,
      backgroundSize: 'cover',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: 848,
      width: '100%'
    },
    colorUnder: {
      color: Color.WHITE
    },
    colorUp: {
      color: Color.ORANGE,
      marginTop: 145
    },
    opa: {
      opacity: 0.5
    },
    containers: {
      backgroundColor: Color.ORANGE,
      color: Color.DISABLED_TEXT,
      width: '100%',
      height: 296,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontWeight: 'normal'
    },
    onItem: {
      height: 262,
      width: '100%',
      backgroundColor: Color.WHITE_LIGHT
    },
    content: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    bottom: {
      paddingBottom: '70px ',
      width: 928
    },
    title: {
      fontFamily: fontFamily,
      color: Color.BLACK,
      fontWeight: 600,
      fontSize: 24,
      lineHeight: '32px',
      padding: '20px 0'
    },
    img: {
      backgroundColor: Color.WHITE_LIGHT,
      margin: '65px 0',
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
    },
    cardList: {
      margin: '65px 0',
      width: '1160px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    df: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    cardListTitle: {
      maxWidth: 736,
      padding: '16px 0px 45px 0px'
    }
  })
)
