import React from 'react'

import { compose } from 'recompose'
import { Grid } from '@material-ui/core'
import { Typography } from '@common/typography'
import Footer from '@common/footer'
import NavMenu from '@common/navMenu'
import { Button } from '@common/button'
import { useStyles } from './style'
import { Icon } from '@common/icon/icon'
import ScrollToTop from 'react-scroll-up'
import { CardItem } from '@common/cardItem'
import { MediaCardList } from '@components/cardsMain'
import Img from '@assets/navMenu/mainUnder.png'
import Img1 from '@assets/main/logo1.png'
import Img2 from '@assets/main/logo2.png'
import Img3 from '@assets/main/logo3.png'
import Img4 from '@assets/main/logo4.png'
import { ReviewMain } from '@components/reviewMain'
import TopMenu from '@components/topMenu'
import { useGlobal } from '@app/theme'
import { Link } from '@common/link/link'

const Main: React.FC = props => {
  useGlobal({})
  const style = useStyles()
  return (
    <Grid className={style.df}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <div className={style.container}>
        <TopMenu hasOpacity={true} />
        <NavMenu hasOpacity={true} />
        <div>
          <Typography className={style.colorUp} variant="h1">
            сварочные работы
          </Typography>
        </div>
        <div>
          <Typography className={style.colorUnder} variant="h1">
            любой сложности в одессе
          </Typography>
        </div>
        <Icon
          variant="main"
          style={{
            width: '928',
            height: '144',
            opacity: 0.2,
            padding: '110 0 0 0',
            color: '#FFFFFF'
          }}
        />
      </div>
      <CardItem />
      <Grid className={style.onItem} />
      <Grid container className={style.cardList}>
        <Typography variant="h2">виды сварочных услуг</Typography>
        <Typography className={style.cardListTitle} variant="body1">
          «Weldes» – это выполнение сложных сварочных работ профессионально,
          качественно и оперативно. Мы предлагаем широкий спектр услуг по
          сварочно-монтажным работам:
        </Typography>
        <MediaCardList />
      </Grid>

      <Grid item className={style.containers}>
        <Typography variant="h2">Нужны другие сварочные работы?</Typography>
        <Typography variant="h3">
          Наши менеджеры проконсультируют по интересующим Вас вопросам!
        </Typography>
        <Typography variant="h3">
          Звоните:+38 (000) 000-00-00, +38 (000) 000-00-00, +38 (000) 000-00-00
        </Typography>
      </Grid>
      <img src={Img} alt="imageTwo" />
      <Grid item className={style.content}>
        <ReviewMain />
        <Grid container className={style.img}>
          <img src={Img1} alt="logo" />
          <img src={Img2} alt="logo" />
          <img src={Img3} alt="logo" />
          <img src={Img4} alt="logo" />
          <img src={Img1} alt="logo" />
          <img src={Img2} alt="logo" />
        </Grid>
        <Grid item className={style.bottom}>
          <Typography className={style.title}>
            «Weldes» - сварочные услуги в Одессе
          </Typography>
          <Typography variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.{' '}
          </Typography>
          <Link href={'/info'}>Читать дальше</Link>
        </Grid>
      </Grid>
      <Footer />
    </Grid>
  )
}
export default compose()(Main)
