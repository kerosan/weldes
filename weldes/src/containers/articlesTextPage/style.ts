import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: 928,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      margin: '60px 0'
    },
    flex: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    content: {
      width: '936px',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
    },
    item: {
      padding: 4,
      '&:hover': {
        backgroundColor: Color.ORANGE
      }
    },
    items: {
      backgroundColor: Color.WHITE,
      height: 350
    },
    text: {
      margin: '20px 0'
    },
    title: {
      margin: '15px 0'
    }
  })
)
