import React from 'react'

import { compose } from 'recompose'
import { Grid } from '@material-ui/core'
import UpMenu from '@components/topMenu'
import Footer from '@common/footer'
import NavMenu from '@common/navMenu'
import { UnderNav } from '@common/underNawMenu'
import Img from '@assets/navMenu/article.png'
import { Button } from '@common/button'
import { useStyles } from './style'
import { Icon } from '@common/icon/icon'
import Breadcrumbs from '@common/breadcrumbs'
import ScrollToTop from 'react-scroll-up'
import { OL, Typography, UL } from '@common/typography'
import { Color } from '@app/theme'
import Img1 from '@assets/articlesTextPageImg/image_1.png'
import Img2 from '@assets/articlesTextPageImg/image_2.png'
import Img3 from '@assets/articlesTextPageImg/image_3.png'
import Img4 from '@assets/articlesTextPageImg/image_4.png'
import Img6 from '@assets/articlesTextPageImg/image_6.png'
import Img7 from '@assets/articlesTextPageImg/imageItem.png'
import MediaCard from '@common/service'
import { MediaCardProps } from '@common/service/types'
import Table from '@app/common/table'

const data1: MediaCardProps[] = [
  {
    img: Img7,
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д. Так как цель применения такого текста исключительно демонстрационная, то и смысловую нагрузку ему нести совсем необязательно. Более того, нечитабельность текста сыграет на руку при оценке качества восприятия макета. Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название...'
  }
]
const data2: MediaCardProps[] = [
  {
    img: Img7,
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д. Так как цель применения такого текста исключительно демонстрационная, то и смысловую нагрузку ему нести совсем необязательно. Более того, нечитабельность текста сыграет на руку при оценке качества восприятия макета. Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название...',
    reverse: true
  }
]
const ArticlesTextPage = props => {
  const style = useStyles()
  return (
    <Grid className={style.flex}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <UpMenu />
      <NavMenu />
      <UnderNav label={'статьи ( текстовая страница )'} img={Img} />
      <Breadcrumbs />
      <Grid item className={style.root}>
        <Grid className={style.title} item>
          <Typography variant="caption">
            Заголовок CAPSION для текстовой страницы может занимать несколько
            строк
          </Typography>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
        </Grid>
        <Grid className={style.title} item>
          <Typography variant="h3">
            Заголовок H3 для текстовой страницы может занимать несколько строк
          </Typography>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
          <OL>
            <li>Ordered list item</li>
            <li>Ordered list item</li>
            <li>Ordered list item two lines</li>
            <li>Ordered list item</li>
            <li>Ordered list item</li>
          </OL>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
        </Grid>
        <Grid className={style.title} item>
          <Typography variant="h4">
            Заголовок H4 для текстовой страницы может занимать несколько строк
          </Typography>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
          <UL>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item two lines
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
          </UL>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
        </Grid>
        <Grid className={style.title} item>
          <Typography variant="h5">
            Заголовок H5 для текстовой страницы может занимать несколько строк
          </Typography>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
          <UL>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item two lines
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
            <li>
              <Icon
                style={{ padding: '0 16px 0 0', width: '16px' }}
                variant={'check'}
                fill={Color.ORANGE}
              />
              Unordered list item
            </li>
          </UL>
          <Typography className={style.text} variant="body1">
            Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
            несмотря на название, не имеет никакого отношения к обитателям
            водоемов. Используется он веб-дизайнерами для вставки на
            интернет-страницы и демонстрации внешнего вида контента, просмотра
            шрифтов, абзацев, отступов и т.д. Так как цель применения такого
            текста исключительно демонстрационная, то и смысловую нагрузку ему
            нести совсем необязательно. Более того, нечитабельность текста
            сыграет на руку при оценке качества восприятия макета.
          </Typography>
        </Grid>

        <Grid className={style.content} container>
          <Grid className={style.item} item>
            <img src={Img1} alt="photoImg" />
          </Grid>
          <Grid className={style.item} item>
            <img src={Img2} alt="photoImg" />
          </Grid>
          <Grid className={style.item} item>
            <img src={Img3} alt="photoImg" />
          </Grid>
          <Grid className={style.item} item>
            <img src={Img4} alt="photoImg" />
          </Grid>
          <Grid className={style.item} item>
            <img src={Img1} alt="photoImg" />
          </Grid>
          <Grid className={style.item} item>
            <img src={Img6} alt="photoImg" />
          </Grid>
          <Grid item>
            <Typography className={style.text} variant="body1">
              Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
              несмотря на название, не имеет никакого отношения к обитателям
              водоемов. Используется он веб-дизайнерами для вставки на
              интернет-страницы и демонстрации внешнего вида контента, просмотра
              шрифтов, абзацев, отступов и т.д. Так как цель применения такого
              текста исключительно демонстрационная, то и смысловую нагрузку ему
              нести совсем необязательно. Более того, нечитабельность текста
              сыграет на руку при оценке качества восприятия макета.
            </Typography>
          </Grid>
          <iframe
            title="video"
            width="928"
            height="522"
            src="https://www.youtube.com/embed/tE0tyh2fQwM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
          <Grid item>
            <Typography className={style.text} variant="body1">
              Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
              несмотря на название, не имеет никакого отношения к обитателям
              водоемов. Используется он веб-дизайнерами для вставки на
              интернет-страницы и демонстрации внешнего вида контента, просмотра
              шрифтов, абзацев, отступов и т.д. Так как цель применения такого
              текста исключительно демонстрационная, то и смысловую нагрузку ему
              нести совсем необязательно. Более того, нечитабельность текста
              сыграет на руку при оценке качества восприятия макета.
            </Typography>
          </Grid>
          <Grid item>
            {data1.map((value, index) => (
              <Grid item key={index}>
                <MediaCard className={style.items} {...value} />
              </Grid>
            ))}
          </Grid>
          <Grid item>
            <Typography className={style.text} variant="body1">
              Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
              несмотря на название, не имеет никакого отношения к обитателям
              водоемов. Используется он веб-дизайнерами для вставки на
              интернет-страницы и демонстрации внешнего вида контента, просмотра
              шрифтов, абзацев, отступов и т.д. Так как цель применения такого
              текста исключительно демонстрационная, то и смысловую нагрузку ему
              нести совсем необязательно. Более того, нечитабельность текста
              сыграет на руку при оценке качества восприятия макета.
            </Typography>
          </Grid>
          <Grid item>
            {data2.map((value, index) => (
              <Grid item key={index}>
                <MediaCard className={style.items} {...value} />
              </Grid>
            ))}
          </Grid>
          <Grid item>
            <Typography className={style.text} variant="body1">
              Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
              несмотря на название, не имеет никакого отношения к обитателям
              водоемов. Используется он веб-дизайнерами для вставки на
              интернет-страницы и демонстрации внешнего вида контента, просмотра
              шрифтов, абзацев, отступов и т.д. Так как цель применения такого
              текста исключительно демонстрационная, то и смысловую нагрузку ему
              нести совсем необязательно. Более того, нечитабельность текста
              сыграет на руку при оценке качества восприятия макета.
            </Typography>
          </Grid>
          <Grid item>
            <Table />
          </Grid>
          <Grid item>
            <Typography className={style.text} variant="body1">
              Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот,
              несмотря на название, не имеет никакого отношения к обитателям
              водоемов. Используется он веб-дизайнерами для вставки на
              интернет-страницы и демонстрации внешнего вида контента, просмотра
              шрифтов, абзацев, отступов и т.д. Так как цель применения такого
              текста исключительно демонстрационная, то и смысловую нагрузку ему
              нести совсем необязательно. Более того, нечитабельность текста
              сыграет на руку при оценке качества восприятия макета.
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Footer />
    </Grid>
  )
}
export default compose()(ArticlesTextPage)
