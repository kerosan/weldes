import makeStyles from '@material-ui/core/styles/makeStyles'

import createStyles from '@material-ui/core/styles/createStyles'

export const useStyles = makeStyles(() =>
  createStyles({
    content: {
      width: '1120px',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
      // maxWidth: 1000
    },
    title: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    text: {
      padding: '0  28px'
    },
    item: {
      margin: '10px 0'
    },
    button: {
      padding: '8px 12px'
    }
  })
)
