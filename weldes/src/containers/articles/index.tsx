import React from 'react'

import { compose } from 'recompose'
import { Grid } from '@material-ui/core'
import UpMenu from '@components/topMenu'
import Footer from '@common/footer'
import NavMenu from '@common/navMenu'
import { UnderNav } from '@common/underNawMenu'
import Img from '@assets/navMenu/article.png'
import { Button } from '@common/button'
import { useStyles } from './connectMui'
import { Icon } from '@common/icon/icon'
import Breadcrumbs from '@common/breadcrumbs'
import ScrollToTop from 'react-scroll-up'
import { MediaCardList } from '@components/cards'

const Article = props => {
  const style = useStyles()
  return (
    <Grid className={style.title}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <UpMenu />
      <NavMenu />
      <UnderNav label={'статьи'} img={Img} />
      <Breadcrumbs />
      <MediaCardList className={style.content} />
      <Footer />
    </Grid>
  )
}
export default compose()(Article)
