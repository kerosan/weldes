import React from 'react'
import { compose } from 'recompose'
import { Grid } from '@material-ui/core'
import UpMenu from '@components/topMenu'
import Footer from '@common/footer'
import NavMenu from '@common/navMenu'
import { UnderNav } from '@common/underNawMenu'
import Img from '@assets/navMenu/ourWork.png'
import { Button } from '@common/button'
import { useStyles } from './style'
import { Icon } from '@common/icon/icon'
import Breadcrumbs from '@common/breadcrumbs'
import ScrollToTop from 'react-scroll-up'
import Img1 from '@assets/ourWork/1.png'
import Img2 from '@assets/ourWork/two.png'
import Img3 from '@assets/ourWork/three.png'
import Img4 from '@assets/ourWork/four.png'
import Img5 from '@assets/ourWork/five.png'
import Img6 from '@assets/ourWork/six.png'
import Img7 from '@assets/ourWork/seven.png'
import Img9 from '@assets/ourWork/nine.png'
import Img10 from '@assets/ourWork/ten.png'
import { PhotoCard } from '@common/photoCard'
import { PhotoCardProps } from '@common/photoCard/types'

const list: PhotoCardProps[] = [
  {
    img: Img1
  },
  {
    img: Img2
  },
  {
    img: Img3
  },
  {
    img: Img4
  },
  {
    img: Img5
  },
  {
    img: Img6
  },
  {
    img: Img7
  },
  {
    img: Img1
  },
  {
    img: Img9
  },
  {
    img: Img10
  },
  {
    img: Img6
  },
  {
    img: Img4
  }
]

const OurWork = props => {
  const style = useStyles()
  return (
    <Grid className={style.title}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <UpMenu />
      <NavMenu />
      <UnderNav label={'наши работы'} img={Img} />
      <Breadcrumbs />
      <Grid className={style.content} container>
        {list.map((value, index) => (
          <Grid item key={index}>
            <PhotoCard {...value} />
          </Grid>
        ))}
      </Grid>
      <Footer />
    </Grid>
  )
}
export default compose()(OurWork)
