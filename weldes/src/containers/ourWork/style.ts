import makeStyles from '@material-ui/core/styles/makeStyles'

import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    content: {
      width: '1320px',
      // height:750,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
    },
    title: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    text: {
      padding: '0  28px'
    },
    item: {
      '&:hover': {
        backgroundColor: Color.ORANGE
      }
    }
  })
)
