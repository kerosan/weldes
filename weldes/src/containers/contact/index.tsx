import React from 'react'
import Map from '@assets/map.png'

import { compose } from 'recompose'
import { Grid } from '@material-ui/core'
import { Typography } from '@common/typography'
import UpMenu from '@components/topMenu'
import Footer from '@common/footer'
import NavMenu from '@common/navMenu'
import { UnderNav } from '@common/underNawMenu'
import Img from '@assets/navMenu/contacts.png'
import { Button } from '@common/button'
import DialogContent from '@material-ui/core/DialogContent'
import Input from '@common/input'
import { Textarea } from '@common/textarea'
import DialogActions from '@material-ui/core/DialogActions'
import { useDialogStyle, useStyles } from './connectMui'
import { SocialNetwork } from '@common/socialNetwork'
import { Color } from '@app/theme'
import { Icon } from '@common/icon/icon'
import Breadcrumbs from '@common/breadcrumbs'
import ScrollToTop from 'react-scroll-up'

const Contact = props => {
  const style = useStyles()
  const classes = useDialogStyle()
  return (
    <Grid className={style.containers}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <UpMenu />
      <NavMenu />
      <UnderNav label={'Контакты'} img={Img} />
      <Breadcrumbs />
      <Grid className={style.content}>
        <Grid item className={style.title}>
          <Typography variant="h4">Свяжитесь с нами</Typography>
          <Typography className={style.item} variant="body1">
            Мы находимся по адресу: ул. Адмирала Лазарева, 56. Одесса, Украина,
            65007
          </Typography>
          <Grid container className={style.item}>
            <Icon
              style={{ maxWidth: '12px', fill: Color.ORANGE }}
              variant={'button'}
            />
            <Grid className={style.cell} item>
              Viber,WhatsApp
            </Grid>
          </Grid>
          <Grid container className={style.item}>
            <Icon
              style={{
                maxWidth: '12px',
                fill: Color.ORANGE,
                paddingRight: '15px'
              }}
              variant={'message'}
            />
            mr.svarnoy90@gmail.com, zsv1508@gmail.com
          </Grid>
          <Grid item>
            <Grid container>
              <Icon
                style={{
                  maxWidth: '12px',
                  fill: Color.ORANGE,
                  paddingRight: '15px'
                }}
                variant={'phone'}
              />
              +38 (063) 574-10-54
            </Grid>
            <Grid item className={style.text}>
              +38 (063) 574-10-54:
            </Grid>
            <Grid item className={style.text}>
              Денис Сергеевич, мастер
            </Grid>
            <Grid item className={style.item}>
              <Grid item className={style.text}>
                +38 (067) 662-17-16
              </Grid>
              <Grid item className={style.text}>
                Сергей Васильевич, менеджер
              </Grid>
            </Grid>
          </Grid>
          <Grid item className={style.item}>
            <SocialNetwork className={style.text} fill={Color.DISABLED_TEXT} />
          </Grid>
        </Grid>
        <Grid item className={style.title}>
          <DialogContent className={style.title}>
            <Input fullWidth label={'Имя *'} />
            <Input fullWidth label={'Телефон *'} />
            <Input fullWidth label={'E-mail *'} />
            <Textarea fullWidth label={'Примечание'} />
          </DialogContent>

          <DialogActions className={style.button}>
            <Input type={'file'} className={classes.inputFile} />
            <Button dark="true" onClick={props.onClick}>
              отправить
            </Button>
          </DialogActions>
        </Grid>
      </Grid>
      <img
        src={Map}
        alt="Мы находимся по адресу: ул. Адмирала Лазарева, 56. Одесса, Украина, 65007"
      />
      <Footer />
    </Grid>
  )
}
export default compose()(Contact)
