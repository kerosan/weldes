import makeStyles from '@material-ui/core/styles/makeStyles'

import createStyles from '@material-ui/core/styles/createStyles'

export const useStyles = makeStyles(() =>
         createStyles({
           containers: {
             flexDirection: 'column',
             justifyContent: 'center',
             alignItems: 'center'
           },
           content: {
             width: '100%',
             display: 'flex',
             flexDirection: 'row',
             alignItems: 'center',
             justifyContent: 'center'
           },
           title: {
             display: 'flex',
             flexDirection: 'column',
             alignItems: 'flex-start',
             justifyContent: 'center',
             width: 352,
             margin: '25px 0px'
           },
           cell: {
             padding: '0 15px'
           },
           text: {
             padding: '0  28px'
           },
           item: {
             margin: '10px 0',
             flexWrap: 'nowrap'
           },
           button: {
             padding: '8px 25px',
             width: 375
           }
         })
       )
export const useDialogStyle = makeStyles(() =>
  createStyles({
    paper: {
      maxWidth: 640,
      width: 640,
      height: 600,
      padding: '54px 96px',
      boxSizing: 'border-box'
    },
    inputFile: {
      width: 200
    }
  })
)
