import React from 'react'

import { compose } from 'recompose'
import { Grid } from '@material-ui/core'
import { Typography } from '@common/typography'
import UpMenu from '@components/topMenu'
import Footer from '@common/footer'
import NavMenu from '@common/navMenu'
import { Button } from '@common/button'
import { useStyles } from './style'
import { Icon } from '@common/icon/icon'
import ScrollToTop from 'react-scroll-up'
import { Color } from '@app/theme'

const Contact = props => {
  const style = useStyles()
  return (
    <Grid className={style.containers}>
      <ScrollToTop showUnder={160}>
        <Button dark="true">
          <Icon variant={'up'} />
        </Button>
      </ScrollToTop>
      <UpMenu />
      <NavMenu />
      <div className={style.container}>
        {/*<img src={props.img} />*/}
        <div className={style.item}>
          <div className={style.color}>4</div>
          <Icon
            variant="notFound"
            style={{ width: 160, height: 160 }}
            fill={Color.ORANGE}
          />
          <div className={style.color}>4</div>
        </div>
        <div>
          <Typography className={style.colorUnder} variant="h4">
            страница не найдена
          </Typography>
        </div>
      </div>
      <Footer />
    </Grid>
  )
}
export default compose()(Contact)
