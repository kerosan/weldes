import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'
import Img from '@assets/notFound/404.png'

export const useStyles = makeStyles(() =>
  createStyles({
    container: {
      backgroundImage: (props: any) => `url("${Img}")`,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: 534,
      width: '100%'
    },
    containers: {},
    color: {
      color: Color.WHITE,
      fontWeight: 'bold',
      fontSize: 224
    },
    colorUnder: {
      color: Color.WHITE
    },
    bottom: {
      width: 48,
      height: 4,
      backgroundColor: Color.ORANGE,
      margin: 5
    },
    item: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    }
  })
)
