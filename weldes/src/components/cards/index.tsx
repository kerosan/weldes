import React from 'react'
import Img1 from '@assets/cards/image_1.png'
import Img2 from '@assets/cards/image_2.png'
import Img3 from '@assets/cards/image_3.png'
import Img4 from '@assets/cards/image_4.png'
import Img5 from '@assets/cards/image_5.png'
import Img6 from '@assets/cards/image_6.png'

import { MediaCard } from '@common/card'
import { Grid } from '@material-ui/core'
import { MediaCardProps } from '@app/common/card/types'
import { useCustomClasses } from '@common/card/styles'

const list: MediaCardProps[] = [
  {
    img: Img1,
    date: '26.09.2019 / 21:19',
    title: 'Название статьи две строки и обрезаем троеточием...',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
    url: '/post/1'
  },
  {
    img: Img2,
    date: '26.09.2019 / 21:19',
    title: 'Название статьи две строки и обрезаем троеточием...',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
    url: '/post/2'
  },
  {
    img: Img3,
    date: '26.09.2019 / 21:19',
    title: 'Название статьи две строки и обрезаем троеточием...',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
    url: '/post/2'
  },
  {
    img: Img4,
    date: '26.09.2019 / 21:19',
    title: 'Название статьи две строки и обрезаем троеточием...',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
    url: '/post/2'
  },
  {
    img: Img5,
    date: '26.09.2019 / 21:19',
    title: 'Название статьи две строки и обрезаем троеточием...',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
    url: '/post/2'
  },
  {
    img: Img6,
    date: '26.09.2019 / 21:19',
    title: 'Название статьи две строки и обрезаем троеточием...',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
    url: '/post/2'
  }
]

export const MediaCardList = props => {
  const classes = useCustomClasses()
  return (
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="center"
      className={classes.container}
    >
      {list.map((value, index) => (
        <Grid item key={index}>
          <MediaCard {...value} />
        </Grid>
      ))}
    </Grid>
  )
}
