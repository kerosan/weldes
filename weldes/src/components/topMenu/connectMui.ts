import { createStyles } from '@material-ui/core'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { Color, fontFamily } from '@app/theme'

export const useStyles = makeStyles(
  createStyles({
    wrapper: {
      fontFamily,
      color: Color.WHITE,
      width: '100%',
      height: 88,
      backgroundColor: (props: any) =>
        props.hasOpacity ? 'rgba(255,255,255,0.15)' : Color.BLUE
    },
    container: {
      height: '100%'
    },
    logo: {
      flex: '1 1 208px'
    },
    phones: {
      flex: '1 1 300px'
    },
    socialWrapper: {
      flex: '1 1 200px',
      justifyContent: 'flex-end'
    },
    social: {
      marginRight: 0,
      marginLeft: 'auto'
    },
    header: {
      height: '100%'
    },
    cell: {
      padding: '0 15px'
    },
    spacer: {
      flex: '1 1 100%',
      maxWidth: 120
    },
    contacts: {
      flex: '1 1 100%',
      whiteSpace: 'nowrap'
    }
  })
)
