import React from 'react'

import { storiesOf } from '@storybook/react'

import TopMenu from './index'

storiesOf('topMenu', module)
  .addDecorator((story: any) => {
    document.body.style.margin = '0'
    document.body.style.padding = '0'
    document.body.style.outline = '0'
    // document.body.style.background = 'black'

    return story()
  })
  .add('TopMenu home', () => <TopMenu hasOpacity={true} />)
  .add('TopMenu other', () => <TopMenu />)
