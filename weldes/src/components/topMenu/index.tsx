import React from 'react'
import { Grid } from '@material-ui/core'
import { Color } from '@app/theme'
import { SocialNetwork } from '@common/socialNetwork'
import { Separator } from '@common/separator'
import { Icon } from '@common/icon/icon'
import { useStyles } from '@components/topMenu/connectMui'
import { Logo } from '@assets/vector/logo'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'

const TopMenu = ({ hasOpacity = false, className = '' }) => {
  const classes = useStyles({ hasOpacity })

  return (
    <div className={[classes.wrapper, className].join(' ')}>
      <Container className={classes.header}>
        <Grid
          container={true}
          className={classes.container}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item={true} className={classes.logo}>
            <Logo />
          </Grid>
          <Grid item={true} className={classes.phones}>
            <Grid container={true} wrap="nowrap">
              <Icon
                style={{ maxWidth: '12px', fill: Color.DISABLED_TEXT }}
                variant={'mobile'}
              />
              <Grid className={classes.cell} item={true}>
                +380000000000
              </Grid>
              <Separator />
              <Grid className={classes.cell} item={true}>
                +38038473738
              </Grid>
              <Separator />
              <Grid className={classes.cell} item={true}>
                +38038473738
              </Grid>
            </Grid>
            <Grid container={true} wrap="nowrap">
              <Icon
                style={{ maxWidth: '12px', fill: Color.DISABLED_TEXT }}
                variant={'message'}
              />
              <Grid className={classes.cell} item={true}>
                mr.svarnoy90@gmail.com
              </Grid>
              <Box className={classes.spacer} />
              <Grid className={classes.contacts} item={true}>
                ( Viber, WhatsApp )
              </Grid>
            </Grid>
          </Grid>
          <Grid item={true} className={classes.socialWrapper}>
            <SocialNetwork fill={Color.WHITE} className={classes.social} />
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default TopMenu
