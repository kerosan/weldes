import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    closeButton: {
      position: 'absolute',
      top: 24,
      right: 40,
      margin: 0,
      padding: 0,
      color: Color.DISABLED_TEXT
    },
    content: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    title: {
      padding: '0 36px'
    },
    button: {
      padding: '8px 12px'
    },
    closeIcon: {
      wight: 12,
      padding: 8
      // color: `${Color.ORANGE} !important`
    },
    inputFile: {
      minWidth: 80,
      width: '200px'
    }
  })
)
export const useDialogStyle = makeStyles(() =>
  createStyles({
    paper: {
      maxWidth: 640,
      width: 640,
      height: 600,
      padding: '54px 96px',
      boxSizing: 'border-box'
    }
  })
)
