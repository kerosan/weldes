import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button } from '@app/common/button'
import { Typography } from '@app/common/typography'
import { Icon } from '@common/icon/icon'
import { useDialogStyle } from '@components/modal/order/styles'
import { useStyles } from './styles'
import Input from '../../../common/input'
import { Textarea } from '@common/textarea'
import { IProps } from '@components/modal/order/types'

export const OrderWindow: React.FC<IProps> = props => {
  const style = useStyles()
  const classes = useDialogStyle()
  return (
    <Dialog
      classes={classes}
      scroll="body"
      open={props.open}
      onClose={props.onClose}
    >
      <DialogTitle disableTypography className={style.title}>
        <Typography variant="h3">Заказать просчёт</Typography>
        <Button
          onClick={props.onClose}
          close={true}
          className={style.closeButton}
        >
          <Icon className={style.closeIcon} variant={'close'} />
          закрыть
        </Button>
      </DialogTitle>
      <DialogContent className={style.content}>
        <Input fullWidth label={'Имя *'} />
        <Input fullWidth label={'Телефон *'} />
        <Input fullWidth label={'E-mail *'} />
        <Textarea fullWidth label={'Примечание'} />
      </DialogContent>

      <DialogActions className={style.button}>
        <Input type={'file'} className={style.inputFile} />
        <Button dark={true} onClick={props.onClick}>
          отправить
        </Button>
      </DialogActions>
    </Dialog>
  )
}
