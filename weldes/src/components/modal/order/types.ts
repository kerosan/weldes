export interface IProps {
  onClick: () => void
  onClose: () => void
  open: boolean
}
