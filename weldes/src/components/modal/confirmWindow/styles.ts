import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color, fontFamily } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    closeButton: {
      position: 'absolute',
      top: 24,
      right: 40,
      margin: 0,
      padding: 0,
      color: Color.DISABLED_TEXT
    },
    title: {
      padding: '0 36px'
    },
    button: {
      padding: '8px 24px',
      justifyContent: 'flex-start'
    }
  })
)

export const useDialogStyle = makeStyles(() =>
  createStyles({
    paper: {
      maxWidth: 640,
      width: 640,
      padding: '54px 96px',
      boxSizing: 'border-box'
      // height: 316
    }
  })
)
