import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import { Button } from '@app/common/button'
import { Typography } from '@app/common/typography'
import { Icon } from '@common/icon/icon'
import {
  useDialogStyle,
  useStyles
} from '@components/modal/confirmWindow/styles'
import { DialogTitle } from '@material-ui/core'

interface IProps {
  onClick: () => void
  onClose: () => void
  open: boolean
}

export const ConfirmWindow: React.FC<IProps> = props => {
  const style = useStyles()
  const classes = useDialogStyle()
  return (
    <Dialog
      classes={classes}
      scroll="body"
      open={props.open}
      onClose={props.onClose}
    >
      <DialogTitle disableTypography className={style.title}>
        <Typography variant="h3">Сообщение отправленно!</Typography>
        <Button close={true} className={style.closeButton}>
          <Icon style={{ wight: 12, padding: 8 }} variant={'close'} />
          закрыть
        </Button>
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Typography variant="body1">
            Спасибо за обращение! Ваше сообщение успешно отправлено. Наши
            менеджеры свяжутся с Вами в ближайшее время.
          </Typography>
        </DialogContentText>
      </DialogContent>

      <DialogActions className={style.button}>
        <Button className={style.button} onClick={props.onClick}>
          вернутся
        </Button>
      </DialogActions>
    </Dialog>
  )
}
