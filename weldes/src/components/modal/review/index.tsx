import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button } from '@app/common/button'
import { Typography } from '@app/common/typography'
import { Icon } from '@common/icon/icon'
import { useDialogStyle, useStyles } from '@components/modal/review/styles'
import Input from '../../../common/input'
import { Textarea } from '@common/textarea'

interface IProps {
  onClick: () => void
  onClose: () => void
  open: boolean
}

export const ReviewWindow: React.FC<IProps> = props => {
  const style = useStyles()
  const classes = useDialogStyle()
  return (
    <Dialog
      classes={classes}
      scroll="body"
      open={props.open}
      onClose={props.onClose}
    >
      <DialogTitle disableTypography className={style.title}>
        <Typography variant="h3">Оставить отзыв</Typography>
        <Button
          onClick={props.onClose}
          close={true}
          className={style.closeButton}
        >
          <Icon style={{ wight: 12, padding: 8 }} variant="close" />
          закрыть
        </Button>
      </DialogTitle>
      <DialogContent className={style.content}>
        <Input fullWidth label={'Имя *'} />
        <Input fullWidth label={'E-mail *'} />
        <Textarea fullWidth label={'Отзыв'} />
      </DialogContent>

      <DialogActions className={style.button}>
        <Button dark={true} onClick={props.onClick}>
          отправить
        </Button>
      </DialogActions>
    </Dialog>
  )
}
