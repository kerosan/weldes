import React from 'react'
import { Grid } from '@material-ui/core'
import { Separator } from '@app/common/separator'
import { Color } from '@app/theme'
import { Icon } from '@app/common/icon/icon'

export default () => (
  <Grid
    container
    direction="row"
    justify="space-around"
    alignItems="stretch"
    style={{ width: 478 }}
  >
    <Grid item>
      <Icon
        variant="mobile"
        style={{ width: 12, height: 12, fill: Color.DISABLED_TEXT }}
      />
      +38038473738
    </Grid>
    <Grid item>
      <Separator />
    </Grid>
    <Grid item>+38038473738</Grid>
    <Grid item>
      <Separator />
    </Grid>
    <Grid item>+38038473738</Grid>
  </Grid>
)
