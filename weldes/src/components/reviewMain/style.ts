import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyles = makeStyles(
  createStyles({
    containers: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    title: {
      padding: '0px 0px 22px 0px'
    },
    wrapper: {
      padding: 20,
      width: 1120,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between'
    },
    paper: {
      position: 'relative',
      padding: 20,
      margin: 16,
      width: 352
    },
    arrow: {
      width: 0,
      height: 0,
      borderStyle: 'solid',
      borderWidth: '0px 0px 20px 20px',
      borderColor: '#ffffff transparent transparent transparent',
      backgroundColor: Color.WHITE,
      boxShadow:
        'rgba(0, 0, 0, 0.35) 1px 1px 0px 0px, rgb(0, 0, 0) 0px 0px 0px 0px, rgb(0, 0, 0) 0px 0px 0px 0px, rgb(0, 0, 0) 0px 0px 0px 0px',
      transform: 'rotate(45deg)',
      position: 'absolute',
      left: 60,
      bottom: -10
    },
    button: {
      margin: '0px 80px'
    },
    cards: {
      display: 'flex',
      justifyContent: 'space-around',
      width: 1120
    }
  })
)
