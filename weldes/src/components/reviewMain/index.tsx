import React from 'react'
import { Button } from '@app/common/button'
import { Typography } from '@app/common/typography'
import { Icon } from '@common/icon/icon'
import { useStyles } from '@components/reviewMain/style'
import { Grid } from '@material-ui/core'
import { ReviewCard } from '@common/reviewItem'
import { ReviewCardProps } from '@common/reviewItem/types'
import { ReviewWindow } from '@components/modal/review'

const list: ReviewCardProps[] = [
  {
    title: 'Константин Константинович Константинопольский',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д.'
  },
  {
    title: 'Константин Константинопольский, ООО «Промтехстрой»',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...'
  },
  {
    title: 'Константин Константинович Константинопольский',
    description:
      'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...'
  }
]

export const ReviewMain = props => {
  const style = useStyles()
  const [open, toggleOpen] = React.useState(false)

  return (
    <Grid className={style.containers}>
      <Grid container className={style.wrapper}>
        <Typography variant="h2">отзывы о нас</Typography>
        <Grid item>
          <Button className={style.button} onClick={() => toggleOpen(true)}>
            <Icon style={{ wight: 12, padding: 8 }} variant="button" />
            оставить отзыв
          </Button>
          <ReviewWindow
            onClick={console.log}
            onClose={() => toggleOpen(false)}
            open={open}
          />
          <Button dark="true">
            <Icon variant="left" />
          </Button>
          <Button dark="true">
            <Icon variant="right" />
          </Button>
        </Grid>
      </Grid>
      <Grid className={style.cards} container>
        {list.map((value, index) => (
          <Grid item key={index}>
            <ReviewCard {...value} />
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}
