import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'
import { Color, fontFamily } from '@app/theme'
const styles = theme =>
  createStyles({
    root: {
      width: 930,
      display: 'flex',
      justifyContent: 'center'
    },
    item: {
      width: '930px',
      height: '428px'
    },
    title: {
      fontFamily: fontFamily,
      color: Color.BLACK,
      fontWeight: 600,
      fontSize: 24,
      lineHeight: '32px',
      padding: '20px 0'
    },
    bottom: {
      padding: '70px 0'
    }
  })

export default withStyles(styles, { withTheme: true })
