import React from 'react'

import { storiesOf } from '@storybook/react'
import { Icon } from '@app/common/icon/icon'

storiesOf('Atoms. Icons', module).add('default', () => (
  <>
    <Icon variant="button" /> <Icon variant="check" /> <Icon variant="fb" />{' '}
    <Icon variant="instagram" /> <Icon variant="left" />{' '}
    <Icon variant="message" /> <Icon variant="mobile" />{' '}
    <Icon variant="phone" /> <Icon variant="play" /> <Icon variant="right" />{' '}
    <Icon variant="twitter" /> <Icon variant="up" /> <Icon variant="vk" />{' '}
    <Icon variant="close" />
  </>
))
