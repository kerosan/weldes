import React from 'react'

import { storiesOf } from '@storybook/react'
import { Separator } from '@app/common/separator'

storiesOf('Atoms. Separator', module).add('default', () => <Separator />)
