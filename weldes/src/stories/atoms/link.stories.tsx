import React from 'react'
import { MemoryRouter as Router } from 'react-router'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { Link } from '@common/link/link'

storiesOf('Atoms. Link', module)
  .add('default', () => (
    <>
      <Link onClick={action('clicked')} href="/">
        Default /
      </Link>
      <br />
      <Link onClick={action('clicked')} href="//ygggggga.ru">
        Default //ya.ru
      </Link>
      <br />
      <Link onClick={action('clicked')}>Default a</Link>
    </>
  ))
  .add('router', () => (
    <Router>
      <Link onClick={action('clicked')} to="/">
        Default /
      </Link>
      <br />
      <Link onClick={action('clicked')}>Default a</Link>
      <br />
      <Link onClick={action('clicked')} to="/home">
        Default /home
      </Link>
    </Router>
  ))
