import React from 'react'

import { storiesOf } from '@storybook/react'

import { OL, Typography, UL } from '@app/common/typography'
import { ListItem } from '@app/common/listItem/listItem'
import Check from '@assets/icon/check'
import { Link } from '@common/link/link'
import { MemoryRouter as Router } from 'react-router'
import { Color } from '@app/theme'

storiesOf('Atoms. Typography ', module).add('default', () => (
  <>
    <Typography variant="h1">Heading 1</Typography>
    <Typography variant="h2">Heading 2</Typography>
    <Typography variant="h3">Heading 3</Typography>
    <Typography variant="h4">Heading 4</Typography>
    <Typography variant="h5">Heading 5</Typography>
    <Typography variant="caption">Heading 2</Typography>
    <OL>
      <ListItem>List item 1</ListItem>
      <ListItem>List item 2</ListItem>
    </OL>
    <UL>
      {['List item 1', 'List item 2', 'List item 3'].map((item, key) => (
        <ListItem
          key={key}
          icon={<Check fill={Color.ORANGE} style={{ width: 10, height: 10 }} />}
        >
          {item}
        </ListItem>
      ))}
    </UL>
    <Typography variant="body1">
      Это основной стиль текста параграфа. Каждый веб-разработчик знает, что
      такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого
      отношения к обитателям водоемов. Своим появлением Lorem ipsum обязан
      древнеримскому философу Цицерону, ведь именно из его трактата
      средневековый книгопечатник вырвал отдельные фразы и слова, получив
      текст-«рыбу».
    </Typography>
    <Typography variant="body1">
      <Router>
        Это <b>выделенный текст внутри параграфа.</b> Вот так выглядит{' '}
        <Link>ссылка в тексте</Link> параграфа по умолчанию, вот так{' '}
        <Link>ссылка при наведении</Link>, это <Link>ссылка при нажатии</Link>,
        а это <Link>посещенная ссылка</Link>.
      </Router>
    </Typography>
  </>
))
