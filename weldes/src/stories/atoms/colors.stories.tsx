import React from 'react'

import { storiesOf } from '@storybook/react'
import { Box } from '@material-ui/core'
import { Color, fontFamily } from '@app/theme'
import Grid from '@material-ui/core/Grid'

const Circle = props => (
  <Box
    padding={1}
    borderRadius="30px"
    margin={1}
    style={{
      minWidth: 30,
      height: 30,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: Color.GREY,
      textShadow: '0 0 1px #fff',
      boxShadow: '0 0 2px 0px #000'
    }}
    {...props}
  />
)

const Pallet = () => (
  <Grid container style={{ fontFamily }}>
    <Grid item xs={12}>
      <Grid container>
        <Circle bgcolor={Color.WHITE}>White</Circle>
        <Circle bgcolor={Color.ORANGE}>Orange</Circle>
        <Circle bgcolor={Color.BLUE}>Blue</Circle>
        <Circle bgcolor={Color.DIVIDER}>Divider</Circle>
        <Circle bgcolor={Color.DISABLED_TEXT}>Disable Text</Circle>
        <Circle bgcolor={Color.SECONDARY_TEXT}>Secondary Text</Circle>
        <Circle bgcolor={Color.PRIMARY_TEXT}>Primary Text</Circle>
        <Circle bgcolor={Color.ERROR}>Error</Circle>
      </Grid>
    </Grid>
  </Grid>
)

storiesOf('Atoms. Colours', module).add('default', () => <Pallet />, {
  options: {}
})
