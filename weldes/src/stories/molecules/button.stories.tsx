import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { Button } from '@app/common/button'
import { Icon } from '@common/icon/icon'

storiesOf('Molecules. Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>заказать</Button>)
  .add('disabled', () => (
    <Button disabled onClick={action('clicked')}>
      заказать Button
    </Button>
  ))
  .add('dark with text', () => (
    <Button dark={true} onClick={action('clicked')}>
      заказать
    </Button>
  ))
  .add('dark disabled', () => (
    <Button dark={true} disabled onClick={action('clicked')}>
      заказать Button
    </Button>
  ))
  .add('dark with text + icon', () => (
    <Button dark={true} onClick={action('clicked')}>
      <Icon variant={'right'} /> заказать
    </Button>
  ))
  .add('dark with + icon', () => (
    <Button dark={true} onClick={action('clicked')}>
      <Icon variant={'right'} />
    </Button>
  ))
  .add('dark close', () => (
    <Button close={true} onClick={action('clicked')}>
      <Icon variant={'close'} /> закрыть
    </Button>
  ))
