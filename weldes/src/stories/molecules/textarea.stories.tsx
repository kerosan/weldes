import React from 'react'

import { storiesOf } from '@storybook/react'

import { Textarea } from '@common/textarea'

storiesOf('Molecules. Textarea ', module).add('with text', () => <Textarea />)
