import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Service from '@app/common/service/index'
import Img from '@assets/cards/image_1.png'

const serviceProps = {
  img: Img,
  title: 'Газоснабжение',
  description:
    'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб...',
  list: [
    'установка, замена бытовых счетчиков газа s ds f sf dfd f vd fb dgb fg  bfh bsaSD SF GF D DAS  D QD AS A',
    'установка, замена бытовых счетчиков газа',
    'установка, замена бытовых счетчиков газа'
  ]
}
storiesOf('Molecules. Service', module)
  .add('row', () => <Service {...serviceProps} onClick={action('clicked')} />)
  .add('row-rev', () => (
    <Service reverse={true} {...serviceProps} onClick={action('clicked')} />
  ))
