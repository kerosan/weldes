import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import Input from '@app/common/input'

storiesOf('Molecules. Input', module)
  .add('with text error', () => (
    <Input label={'Имя *'} helperText="hello" error={true} />
  ))
  .add('with text', () => <Input label={'Имя *'} helperText="hello" />)
  .add('disabled', () => (
    <Input disabled onClick={action('clicked')}>
      Hellccdo Button
    </Input>
  ))
