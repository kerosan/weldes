import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { UnderNav } from '@app/common/underNawMenu'
import Img from '@assets/navMenu/about us.png'
import { Typography } from '@common/typography'

storiesOf('Molecules. UnderNav', module).add('with text', () => (
  <UnderNav img={Img} label={'О нас'} />
))
