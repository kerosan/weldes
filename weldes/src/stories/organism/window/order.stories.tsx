import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { OrderWindow } from '@components/modal/order'

storiesOf('Molecules. Modal. order', module).add(' text', () => (
  <OrderWindow
    onClick={action('clicked')}
    open={true}
    onClose={action('close')}
  >
    заказать
  </OrderWindow>
))
