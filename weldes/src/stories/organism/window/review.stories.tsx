import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { ReviewWindow } from '@components/modal/review'

storiesOf('Molecules. Modal. reviewMain', module).add('with text', () => (
  <ReviewWindow
    onClick={action('clicked')}
    open={true}
    onClose={action('close')}
  >
    заказать
  </ReviewWindow>
))
