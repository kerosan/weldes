import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { ConfirmWindow } from '@components/modal/confirmWindow'

storiesOf('Molecules. Modal. confirm', module).add('with text', () => (
  <ConfirmWindow
    onClick={action('clicked')}
    open={true}
    onClose={action('close')}
  >
    заказать
  </ConfirmWindow>
))
