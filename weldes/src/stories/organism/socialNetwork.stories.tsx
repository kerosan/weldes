import React from 'react'

import { storiesOf } from '@storybook/react'

import { SocialNetwork } from '@app/common/socialNetwork'
import { Color } from '@app/theme'

storiesOf('Organism. Social Network', module).add('with text', () => (
  <SocialNetwork fill={Color.ORANGE} />
))
