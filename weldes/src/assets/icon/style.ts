import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    icon: {
      '&:hover path': {
        fill: Color.ORANGE
      }
    }
  })
)
