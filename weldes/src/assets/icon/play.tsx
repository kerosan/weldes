import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
export default props => (
  <SvgIcon
    width="164px"
    height="164px"
    viewBox="0 0 164 164"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
    style={{ width: 164, height: 164 }}
  >
    <circle cx="82" cy="82" r="77" stroke="white" strokeWidth="10" />
    <path
      data-fill="none"
      fillRule="evenodd"
      clipRule="evenodd"
      d="M68 60V103.167L101.917 81.5833L68 60Z"
      fill="white"
    />
  </SvgIcon>
)
