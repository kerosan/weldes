import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'

export default props => (
  <SvgIcon
    width="16"
    height="12"
    viewBox="0 0 16 12"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.08471 9.46756L1.29164 5.73602L0 6.99776L5.08471 12L16 1.26174L14.7175 0L5.08471 9.46756Z"
      fill={props.fill}
    />
  </SvgIcon>
)
