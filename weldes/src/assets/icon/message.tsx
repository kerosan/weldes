import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
export default props => (
  <SvgIcon
    width="17"
    height="13"
    viewBox="0 0 17 13"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M14.625 0H1.625C0.73125 0 0.008125 0.73125 0.008125 1.625L0 11.375C0 12.2688 0.73125 13 1.625 13H14.625C15.5188 13 16.25 12.2688 16.25 11.375V1.625C16.25 0.73125 15.5188 0 14.625 0ZM14.625 11.375H1.625V3.25L8.125 7.3125L14.625 3.25V11.375ZM8.125 5.6875L1.625 1.625H14.625L8.125 5.6875Z"
      fill={props.fill}
    />
  </SvgIcon>
)
