import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
export default props => (
  <SvgIcon
    width="8"
    height="12"
    viewBox="0 0 8 12"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0.59 10.59L5.17 6L0.59 1.41L2 0L8 6L2 12L0.59 10.59Z"
      fill={props.fill}
    />
  </SvgIcon>
)
