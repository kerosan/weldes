import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
export default props => (
  <SvgIcon
    width="12"
    height="18"
    viewBox="0 0 12 18"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9 0H2.45455C1.09636 0 0 1.09636 0 2.45455V15.5455C0 16.9036 1.09636 18 2.45455 18H9C10.3582 18 11.4545 16.9036 11.4545 15.5455V2.45455C11.4545 1.09636 10.3582 0 9 0ZM7.36355 16.3638H4.09082V15.5457H7.36355V16.3638ZM10.0228 13.9093H1.43188V2.45471H10.0228V13.9093Z"
      fill={props.fill}
    />
  </SvgIcon>
)
