import React from 'react'
import SvgIcon from '@material-ui/core/SvgIcon'
export default props => (
  <SvgIcon
    width="9"
    height="16"
    viewBox="0 0 9 16"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M9 2.77354H6.444C6.3526 2.77813 6.26375 2.80486 6.18528 2.85136C6.1068 2.89786 6.04112 2.96272 5.994 3.0402C5.85725 3.23232 5.78773 3.46327 5.796 3.69797V5.51127H9V8.14234H5.796V16H2.736V8.14234H0V5.51127H2.736V3.98241C2.71655 2.95055 3.09637 1.95021 3.798 1.18541C4.12418 0.808451 4.53025 0.506896 4.9875 0.302093C5.44474 0.0972902 5.94197 -0.00575572 6.444 0.000248058H9V2.77354Z"
      fill={props.fill}
    />
  </SvgIcon>
)
