import React from 'react'
import { Grid } from '@material-ui/core'
import { Icon } from '@app/common/icon/icon'

export const SocialNetwork: React.FC<{
  fill: string
  className?: string
}> = props => {
  return (
    <Grid
      container
      direction="row"
      justify="space-around"
      alignItems="stretch"
      style={{ width: 192 }}
      {...props}
    >
      <Icon variant="twitter" fill={props.fill} />
      <Icon variant="fb" fill={props.fill} />
      <Icon variant="instagram" fill={props.fill} />
      <Icon variant="vk" fill={props.fill} />
    </Grid>
  )
}
