import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'

const styles = theme =>
  createStyles({
    root: {
      fill: 'orange'
    }
  })

export default withStyles(styles, { withTheme: true })
