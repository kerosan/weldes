import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'
const styles = theme =>
  createStyles({
    container: {
      backgroundColor: Color.PRIMARY_TEXT,
      color: Color.DISABLED_TEXT,
      width: '100%',
      height: 50,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
    },
    cell: {
      padding: '0 15px'
    },
    social: {
      marginBottom: '17px'
    },
    text: {
      margin: 8
    },
    foot: {
      width: '100%',
      height: 160,
      backgroundColor: Color.BLUE,
      color: Color.WHITE
    },
    root: {
      width: '100%',
      backgroundColor: Color.BLUE
    }
  })

export default withStyles(styles, { withTheme: true })
