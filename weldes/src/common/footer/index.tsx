import React from 'react'
import { Grid } from '@material-ui/core'
import { Color } from '@app/theme'
import { SocialNetwork } from '../../common/socialNetwork'
import { compose } from 'recompose'
import connectMui from './connectMui'
import { WithStyles } from '@material-ui/core'
import { Icon } from '@common/icon/icon'
import { Logo } from '@assets/vector/logo'
import Container from '@material-ui/core/Container'

const Footer = props => (
  <div className={props.classes.root}>
    <Container>
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
        className={props.classes.foot}
      >
        <Grid item className={props.classes.logo}>
          <Logo style={{ width: 310, height: 48 }} />
        </Grid>
        <Grid item>
          <Grid className={props.classes.text} item>
            График работы:
          </Grid>
          <Grid className={props.classes.text} item>
            Пн-Пт с 00:00 до 00:00
          </Grid>
          <Grid className={props.classes.text} container>
            <Icon
              style={{
                maxWidth: '12px',
                fill: Color.DISABLED_TEXT,
                paddingRight: '15px'
              }}
              variant={'message'}
            />
            mr.svarnoy90@gmail.com
          </Grid>
        </Grid>
        <Grid item>
          <Grid className={props.classes.text} item>
            +38 (000) 000-00-00
          </Grid>
          <Grid className={props.classes.text} item>
            +38 (000) 000-00-00
          </Grid>
          <Grid className={props.classes.text} item>
            +38 (000) 000-00-00
          </Grid>
        </Grid>
        <Grid item>
          <SocialNetwork className={props.classes.social} fill={Color.WHITE} />
          <Grid container>
            <Icon
              style={{ maxWidth: '12px', fill: Color.ORANGE }}
              variant={'button'}
            />
            <Grid className={props.classes.cell} item>
              (Viber,WhatsApp)
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Container>
    <Grid className={props.classes.container} item>
      Все права защищены © 2014-20ХХ WELDES
    </Grid>
  </div>
)
export default compose<WithStyles>(connectMui)(Footer)
