import { Typography as Text } from '@material-ui/core'
import React from 'react'
import { useCustomStyles, useStyles } from '@common/typography/styles'
import { TypographyProps } from '@material-ui/core/Typography'

export const Typography: React.FC<TypographyProps> = props => {
  const classes = useStyles()

  return <Text {...props} classes={classes} />
}

export const UL = props => {
  const classes = useStyles()
  const customClasses = useCustomStyles()

  return (
    <ul
      {...props}
      className={[props.className, classes.body1, customClasses.ul].join(' ')}
    />
  )
}

export const OL = props => {
  const classes = useStyles()
  const customClasses = useCustomStyles()

  return (
    <ol
      {...props}
      className={[props.className, classes.body1, customClasses.ol].join(' ')}
    />
  )
}
