import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color, fontFamily } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      '& b': {
        fontFamily: fontFamily,
        color: Color.SECONDARY_TEXT,
        fontWeight: 600,
        fontSize: 16,
        lineHeight: '24px'
      }
    },
    h1: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 'bold',
      fontSize: 48,
      lineHeight: '72px',
      textTransform: 'uppercase'
    },
    h2: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 'bold',
      fontSize: 34,
      lineHeight: '150%',
      textTransform: 'uppercase'
    },
    h3: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 600,
      fontSize: 24,
      lineHeight: '51px'
    },
    h4: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 600,
      fontSize: 20,
      lineHeight: '32px'
    },
    h5: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 600,
      fontSize: 16,
      lineHeight: '24px'
    },
    body1: {
      fontFamily: fontFamily,
      color: Color.SECONDARY_TEXT,
      fontWeight: 'normal',
      fontSize: 16,
      lineHeight: '24px'
    },
    body2: {
      fontFamily: fontFamily,
      color: Color.SECONDARY_TEXT,
      fontWeight: 'normal',
      fontSize: 14,
      lineHeight: '20px'
    },
    caption: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 'bold',
      fontSize: 34,
      lineHeight: '51px'
    }
  })
)

export const useCustomStyles = makeStyles(
  createStyles({
    ul: {
      fontFamily: fontFamily,
      color: Color.GREY,
      fontWeight: 'normal',
      fontSize: 16,
      lineHeight: '24px',
      listStyleType: 'none',
      paddingLeft: 0
    },
    ol: {
      fontFamily: fontFamily,
      color: Color.GREY,
      fontWeight: 'normal',
      fontSize: 16,
      lineHeight: '24px',
      listStyleType: 'decimal',
      paddingLeft: 20
    }
  })
)
