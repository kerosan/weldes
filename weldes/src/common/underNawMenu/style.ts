import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyle = makeStyles(
  createStyles({
    container: {
      backgroundImage: (props: any) => `url("${props.img}")`,
      backgroundSize: 'cover',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: 300,
      width: '100%'
    },
    color: {
      // width:48,
      color: Color.WHITE
    },
    bottom: {
      width: 48,
      height: 4,
      backgroundColor: Color.ORANGE,
      margin: 5
    }
  })
)
