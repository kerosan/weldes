import React from 'react'
import { useStyle } from '@app/common/underNawMenu/style'
import { Typography } from '@common/typography'

export const UnderNav: React.FC<{ img: string; label: string }> = props => {
  const classes = useStyle({ img: props.img })
  return (
    <div className={classes.container}>
      {/*<img src={props.img} />*/}
      <Typography className={classes.color} variant="h2">
        {props.label}
      </Typography>
      <div className={classes.bottom} />
    </div>
  )
}
