import React from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Breadcrumbs from '@material-ui/core/Breadcrumbs'
import Link from '@material-ui/core/Link'
import { useStyles } from './style'

function handleClick(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
  event.preventDefault()
  alert('You clicked a breadcrumb.')
}

export default function SimpleBreadcrumbs() {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Paper elevation={0} className={classes.paper}>
        <Breadcrumbs aria-label="Breadcrumb">
          <Link className={classes.text} href="/" onClick={handleClick}>
            Главная
          </Link>
          <Link
            className={classes.text}
            href="/getting-started/installation/"
            onClick={handleClick}
          >
            Директория
          </Link>
          <Typography color="textPrimary">Текущяя страница</Typography>
        </Breadcrumbs>
      </Paper>
    </div>
  )
}
