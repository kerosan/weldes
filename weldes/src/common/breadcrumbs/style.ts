import { createStyles, makeStyles, Theme } from '@material-ui/core'
import { Color } from '@app/theme'

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexWrap: 'wrap',
      width: '100%',
      height: 56,
      backgroundColor: Color.WHITE_LIGHT
    },
    paper: {
      backgroundColor: Color.WHITE_LIGHT,
      justifyContent: 'center',
      margin: '0 300px'
    },
    text: {
      color: Color.ORANGE
    }
  })
)
