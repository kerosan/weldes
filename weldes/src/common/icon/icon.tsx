import * as React from 'react'
import { useStyles } from '@common/icon/style'
import { icons } from '@common/icon/assets'

type Variant = keyof typeof icons

export const Icon: React.FC<{
  variant: Variant
  className?: string
  [key: string]: any
}> = ({ variant, ...props }) => {
  const classes = useStyles()
  const CustomIcon = icons[variant]
  return (
    <CustomIcon
      {...props}
      className={[classes.icon, props.className].join(' ')}
    />
  )
}
