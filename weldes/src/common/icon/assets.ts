import button from '@assets/icon/button'
import check from '@assets/icon/check'
import fb from '@assets/icon/fb'
import instagram from '@assets/icon/instagram'
import left from '@assets/icon/left'
import message from '@assets/icon/message'
import play from '@assets/icon/play'
import right from '@assets/icon/right'
import mobile from '@assets/icon/mobile'
import phone from '@assets/icon/phone'
import twitter from '@assets/icon/twitter'
import up from '@assets/icon/up'
import close from '@assets/icon/close'
import vk from '@assets/icon/vk'
import main from '@assets/icon/main'
import cardItem1 from '@assets/icon/cardItem1'
import cardItem2 from '@assets/icon/cardItem2'
import cardItem3 from '@assets/icon/cardItem3'
import notFound from '@assets/icon/notFound'

export const icons = {
  button,
  check,
  fb,
  instagram,
  left,
  message,
  mobile,
  phone,
  play,
  right,
  twitter,
  up,
  vk,
  close,
  main,
  cardItem1,
  cardItem2,
  cardItem3,
  notFound
}
