import * as React from 'react'
import { useStyles } from '@app/common/listItem/styles'

export const ListItem = props => {
  const classes = useStyles()

  return (
    <li className={classes.li}>
      {props.icon || null} {props.children}
    </li>
  )
}
