import React from 'react'
import { compose } from 'recompose'
import connectMui from './connectMui'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'

const MuiInput = (props: TextFieldProps) => (
  <TextField
    label={props.label}
    //   classes={props.classes}
    // className={props.classes.textField}
    margin="dense"
    variant={'outlined' as any}
    {...props}
    id="custom-css-outlined-input"
  />
)

export default compose(connectMui)(MuiInput)
