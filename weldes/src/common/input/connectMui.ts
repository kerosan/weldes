import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'

const styles = theme =>
  createStyles({
    outlined: {
      color: Color.GREY,
      minWidth: 100,
      minHeight: 39,
      margin: '12px',
      // '& label.Mui-focused': {
      //   color: Color.ORANGE
      // },
      // '& .MuiOutlinedInput-root': {
      //   '& fieldset': {
      //     borderColor: Color.DIVIDER
      //   },
      //   '&:hover fieldset': {
      //     borderColor: Color.DISABLED_TEXT
      //   },
      //   '& $focused fieldset': {
      //     borderColor: Color.ORANGE
      //   }
      // }
      '&$focused': {
        borderColor: Color.ORANGE
      }
    },
    focused: {},
    error: {
      '& label.Mui-focused': {
        color: Color.ERROR
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: Color.ERROR
        },
        '&:hover fieldset': {
          borderColor: Color.ERROR
        },
        '&.Mui-focused fieldset': {
          borderColor: Color.ERROR
        }
      }
    },
    disabled: {
      borderColor: Color.WHITE_DARK,
      opacity: 0.5
    },
    textField: {
      borderColor: Color.ORANGE
      // border: '1px solid '
    }
  })

export default withStyles(styles, { withTheme: true })
