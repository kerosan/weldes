import { createStyles, makeStyles, Theme } from '@material-ui/core'
import { Color } from '@app/theme'

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 930,
      marginTop: theme.spacing(3),
      overflowX: 'auto'
    },
    table: {
      minWidth: 650
    },
    bg: {
      '&:nth-child(even)': {
        backgroundColor: Color.DIVIDER
      }
    }
  })
)
