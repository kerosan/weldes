import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { Typography } from '@common/typography'
import { useStyles } from '@common/table/style'

function createData(name: string, unitOfMeasurement: string, price: string) {
  return { name, unitOfMeasurement, price }
}

const rows = [
  createData(
    'Каждый веб-разработчик знает, что такое текст-«рыба»',
    'шт',
    'от 250 грн'
  ),
  createData('Текст-«рыба»', '1 м2', 'от 20 грн'),
  createData('Каждый веб-разработчик знает', 'п.м.', 'от 4000 грн'),
  createData('Каждый веб-разработчик', 'выезд', 'договорная'),
  createData('Каждый веб-разработчик знает, что такое', '—', 'от 2500 грн')
]

export default function SimpleTable() {
  const classes = useStyles()

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>
              <Typography variant="h5">Наименование услуги</Typography>{' '}
            </TableCell>
            <TableCell align="right">
              <Typography variant="h5">Ед. измерения</Typography>
            </TableCell>
            <TableCell align="right">
              <Typography variant="h5">Цена</Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow className={classes.bg} key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.unitOfMeasurement}</TableCell>
              <TableCell align="right">{row.price}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  )
}
