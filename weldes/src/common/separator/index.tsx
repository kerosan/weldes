import React from 'react'
import { Color } from '@app/theme'

const style = {
  background: Color.GREY,
  width: 1,
  height: 16,
  margin: 'auto'
}
export const Separator = () => <div style={style} />
