export interface MediaCardProps {
  img: string
  date: any
  title: string
  description: string
  url: string
  list: string[]
}
