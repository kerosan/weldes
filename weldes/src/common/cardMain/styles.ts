import { createStyles } from '@material-ui/core'
import { Color, fontFamily } from '@app/theme'
import makeStyles from '@material-ui/core/styles/makeStyles'
export const useCustomClasses = makeStyles(
  createStyles({
    root: {
      position: 'relative',
      margin: '0 32px 32px 0',
      fontFamily,

      '& img': {
        width: '100%',
        height: 208
      },
      width: 352,
      maxHeight: 528,
      minHeight: 528,
      boxShadow:
        '8px 8px 0px rgba(250, 152, 62), 0px 2px 2px rgba(0, 0, 0, 0.12), 0px 1px 3px rgba(0, 0, 0, 0.2)',
      borderRadius: 0,
      '&:hover': {
        backgroundColor: Color.WHITE,
        boxShadow:
          '8px 8px 0px rgba(250, 152, 62),  0px 12px 17px rgba(0, 0, 0, 0.14), 0px 5px 22px rgba(0, 0, 0, 0.12), 0px 7px 8px rgba(0, 0, 0, 0.2)'
      },
      '&:hover $date': {
        color: Color.ORANGE
      }
    },
    image: {
      height: 208,
      position: 'relative'
    },
    date: {
      fontSize: 24,
      lineHeight: '32px',
      fontWeight: 300,
      color: Color.WHITE,
      fontFamily,
      position: 'absolute',
      left: 24,
      bottom: 19,
      userSelect: 'none'
    },
    title: {
      fontFamily: fontFamily,
      color: Color.WHITE,
      fontWeight: 600,
      fontSize: 20,
      lineHeight: '32px'
    },
    li: {
      padding: '8px 0'
    },
    bottom: {
      width: 48,
      height: 4,
      backgroundColor: Color.ORANGE,
      margin: '17px 0'
    }
  })
)
