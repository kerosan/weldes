import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import { UL } from '@app/common/typography'
import Card from '@material-ui/core/Card'
import React from 'react'
import { useCustomClasses } from './styles'
import { MediaCardProps } from '@app/common/cardMain/types'
import { Icon } from '@common/icon/icon'
import { Color } from '@app/theme'

export const MediaCard: React.FC<MediaCardProps> = props => {
  const classes = useCustomClasses()
  const { date, title, image, ...restClasses } = classes
  return (
    <Card {...props} classes={restClasses}>
      <CardMedia className={image} image={props.img}>
        <span className={date}>
          {props.date}
          <div className={classes.bottom} />
        </span>
      </CardMedia>
      <CardContent>
        <UL>
          {props.list &&
            props.list.map((item, index) => (
              <li key={index} className={classes.li}>
                <Icon
                  style={{ padding: '0 16px', width: '16px' }}
                  variant={'check'}
                  fill={Color.ORANGE}
                />
                {item}
              </li>
            ))}
        </UL>
      </CardContent>
    </Card>
  )
}
