import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'

const styles = theme =>
  createStyles({
    root: {
      color: Color.BLACK,
      fontWeight: 'bold',
      padding: '32px',
      textTransform: 'uppercase',
      '&:hover': {
        borderBottom: '2px solid #FA983E',
        color: Color.ORANGE
      },
      '&:active': {
        borderBottom: '2px solid #FA983E',
        color: Color.ORANGE
      }
    }
  })

export default withStyles(styles, { withTheme: true })
