import { createStyles } from '@material-ui/core'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { Color, fontFamily } from '@app/theme'

export const useStyles = makeStyles(
  createStyles({
    wrapper: {
      fontFamily,
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      boxShadow: 'none',
      border: '1px solid #666666',
      backgroundColor: (props: any) =>
        props.hasOpacity ? 'rgba(255,255,255,0.15)' : Color.WHITE
    },
    cell: {
      // width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      boxShadow: 'none',
      marginRight: 250
    },
    flexContainer: {
      display: 'flex',
      alignItems: 'center',
      boxShadow: 'none'
    },
    body: {
      fontFamily: fontFamily,
      color: (props: any) =>
        props.hasOpacity ? Color.WHITE : Color.PRIMARY_TEXT,
      fontWeight: 600,
      fontSize: 14,
      lineHeight: '20px',
      '&:hover': {
        color: Color.ORANGE,
        boxShadow: 'none'
      }
    },
    sell: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    separator: {
      // borderRight: '1px solid #FA983E',
      height: 68,
      minWidth: '105px',
      fontFamily: fontFamily,
      color: (props: any) =>
        props.hasOpacity ? Color.WHITE : Color.PRIMARY_TEXT,
      fontWeight: 'bold',
      fontSize: 14,
      lineHeight: '20px',
      opacity: 1,
      '&:hover': {
        color: Color.ORANGE,
        boxShadow: 'none'
      }
    },
    indicator: {
      backgroundColor: Color.ORANGE
    }
  })
)
