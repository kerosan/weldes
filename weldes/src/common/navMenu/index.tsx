import React from 'react'
import { Paper } from '@material-ui/core'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import { useStyles } from '@common/navMenu/connectMui'

import { Button } from '@common/button'
import { ROUTES } from '@app/router/constants'
import { OrderWindow } from '@components/modal/order'
import { Separator } from '@common/separator'

const NavMenu = ({ hasOpacity = false, className = '' }) => {
  const [value, setValue] = React.useState(0)

  function handleChange(event: React.ChangeEvent<{}>, newValue: number) {
    setValue(newValue)
  }
  const [open, toggleOpen] = React.useState(false)
  const style = useStyles({ hasOpacity })
  return (
    <Paper className={[style.wrapper, className].join(' ')}>
      <Tabs
        classes={{ root: style.cell, indicator: style.indicator }}
        value={value}
        onChange={handleChange}
      >
        <Tab className={style.separator} href={'/'} label={'главная'} />
        <Separator />
        <Tab className={style.separator} href={ROUTES.ABOUT} label={'О нас'} />
        <Separator />
        <Tab
          className={style.separator}
          href={ROUTES.SERVICE}
          label={'услуги'}
        />{' '}
        <Separator />
        <Tab
          className={style.separator}
          href={ROUTES.PRICE}
          label={'цены'}
        />{' '}
        <Separator />
        <Tab
          className={style.separator}
          href={ROUTES.OUR_WORK}
          label={'наши работы'}
        />
        <Separator />
        <Tab
          className={style.separator}
          href={ROUTES.ARTICLES}
          label={'статьи'}
        />
        <Separator />
        <Tab
          className={style.separator}
          href={ROUTES.CONTACT}
          label={'контакты'}
        />
        <OrderWindow
          onClick={console.log}
          onClose={() => toggleOpen(false)}
          open={open}
        />
      </Tabs>
      <Button onClick={() => toggleOpen(true)} dark="true">
        заказать просчёт
      </Button>
    </Paper>
  )
}
export default NavMenu
