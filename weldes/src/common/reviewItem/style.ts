import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'
import makeStyles from '@material-ui/core/styles/makeStyles'
export const useCustomClasses = makeStyles(
  createStyles({
    paper: {
      position: 'relative',
      padding: 20,
      margin: 16,
      width: 352
    },
    title: {
      padding: '0px 0px 22px 0px',
      color: Color.BLACK
    },
    arrow: {
      width: 0,
      height: 0,
      borderStyle: 'solid',
      borderWidth: '0px 0px 20px 20px',
      borderColor: '#ffffff transparent transparent transparent',
      backgroundColor: Color.WHITE,
      boxShadow:
        'rgba(0, 0, 0, 0.35) 1px 1px 0px 0px, rgb(0, 0, 0) 0px 0px 0px 0px, rgb(0, 0, 0) 0px 0px 0px 0px, rgb(0, 0, 0) 0px 0px 0px 0px',
      transform: 'rotate(45deg)',
      position: 'absolute',
      left: 60,
      bottom: -10
    },
    card: {
      maxWidth: 352
    }
  })
)
