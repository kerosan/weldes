export interface ReviewCardProps {
  title: string
  description: string
}
