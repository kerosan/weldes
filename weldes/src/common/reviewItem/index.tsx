import { Typography } from '@app/common/typography'
import React from 'react'
import { useCustomClasses } from './style'
import { ReviewCardProps } from '@app/common/reviewItem/types'
import Box from '@material-ui/core/Box'
import { Grid } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import { Link } from '@common/link/link'

export const ReviewCard: React.FC<ReviewCardProps> = props => {
  const classes = useCustomClasses()
  return (
    <Grid className={classes.card} container>
      <Paper className={classes.paper}>
        <Typography className={classes.title} variant="body1">
          {props.title}
        </Typography>
        <Typography variant="body2">{props.description}</Typography>
        <Link href={'/info'}>Читать полностью</Link>
        <Box className={classes.arrow} />
      </Paper>
    </Grid>
  )
}
