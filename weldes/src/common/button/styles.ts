import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color, fontFamily } from '@app/theme'

export const useButtonStyle = makeStyles(() =>
  createStyles({
    root: {
      minWidth: '12px',
      borderRadius: 'none'
    },
    contained: {
      height: 36,
      borderWidth: 2,
      backgroundColor: Color.ORANGE,
      color: Color.WHITE,
      borderRadius: 3,
      margin: '12px',
      fontFamily,
      letterSpacing: '0.03em',
      boxShadow: 'none',
      fontSize: 14,
      '& svg': {
        width: 12,
        borderRadius: 'none',
        height: 12
      },
      '&:hover': {
        backgroundColor: Color.ORANGE,
        color: Color.WHITE,
        opacity: 0.9,
        boxShadow: 'none'
      },
      '&:active': {
        boxShadow: 'none',
        opacity: 0.7,
        backgroundColor: Color.ORANGE,
        color: Color.WHITE
      }
    },
    disabled: {
      // '&$outlined': {
      //   backgroundColor: Color.DISABLED_TEXT,
      //   color: Color.GREY
      // }
    }
  })
)
export const useThemeStyle = makeStyles(() =>
  createStyles({
    dark: {
      backgroundColor: Color.ORANGE,
      color: Color.WHITE,
      '&:hover': {
        backgroundColor: Color.ORANGE,
        color: Color.WHITE,
        opacity: 0.9,
        boxShadow: 'none'
      },
      '&:active': {
        boxShadow: 'none',
        opacity: 0.7,
        backgroundColor: Color.ORANGE,
        color: Color.WHITE
      }
    },
    close: {
      backgroundColor: Color.TRANSPARENT,
      color: Color.DISABLED_TEXT,
      '&:hover': {
        backgroundColor: Color.TRANSPARENT,
        color: Color.ORANGE,
        boxShadow: 'none'
      }
    },
    white: {
      backgroundColor: Color.TRANSPARENT,
      color: Color.ORANGE,
      '&:hover': {
        backgroundColor: Color.GREY_HOVER,
        color: Color.ORANGE,
        opacity: 0.9,
        boxShadow: 'none'
      },
      '&:active': {
        boxShadow: 'none',
        opacity: 0.7,
        backgroundColor: Color.GREY_ACTIVE,
        color: Color.ORANGE
      }
    }
  })
)
