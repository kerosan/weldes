import React from 'react'
import { Button as ButtonMui } from '@material-ui/core'
import { useButtonStyle, useThemeStyle } from '@app/common/button/styles'

export const Button = props => {
  const classes = useButtonStyle()
  const theme = useThemeStyle()
  const themeClass = props.dark
    ? [theme.dark, props.className].join(' ')
    : props.close
    ? [theme.close, props.className].join(' ')
    : [theme.white, props.className].join(' ')

  return (
    <ButtonMui
      variant="contained"
      disableRipple={true}
      children={props.children}
      classes={classes}
      className={themeClass}
    />
  )
}
