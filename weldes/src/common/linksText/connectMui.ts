import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'

const styles = theme =>
  createStyles({
    root: {
      color: Color.ORANGE,

      '&:hover': {
        color: Color.ORANGE,
        textDecoration: 'underline'
      },
      '&:active': {
        color: Color.ORANGE_DARK
      },
      '&:visited': {
        color: Color.BLACK,
        textDecoration: 'underline'
      }
    }
  })

export default withStyles(styles, { withTheme: true })
