import React from 'react'
import { compose } from 'recompose'
import connectMui from './connectMui'
import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link'

const MyLink = props => <RouterLink to="/" {...props} />

const MuiLink = props => (
  <Link
    underline="none"
    classes={props.classes}
    component={MyLink}
    {...props}
  />
)

export default compose(connectMui)(MuiLink)
