import CardContent from '@material-ui/core/CardContent'
import { Typography } from '@app/common/typography'
import React from 'react'
import { useCardClasses, useCustomClasses } from './styles'
import { Card } from '@material-ui/core'
import { Icon } from '@common/icon/icon'
import { Color } from '@app/theme'

export const CardItem = props => {
  const classes = useCustomClasses()
  const cardClasses = useCardClasses()
  const { title, ...restClasses } = classes
  return (
    <div className={classes.conteiners}>
      <Card {...props} classes={cardClasses}>
        <CardContent className={classes.item}>
          <Icon
            variant="cardItem1"
            style={{ width: '74', height: '80', color: Color.ORANGE }}
          />
          <div>
            <Typography variant="h4">Контроль</Typography>{' '}
            <Typography variant="h4">качества</Typography>
          </div>
          <Typography variant="body1">
            Мы заботимся о своей репутации, поэтому клиенты получают только
            качественные изделия.
          </Typography>
        </CardContent>
      </Card>
      <Card className={classes.itemMiddleBg} {...props} classes={restClasses}>
        <CardContent className={classes.itemMiddle}>
          <Icon
            variant="cardItem2"
            style={{ width: '74', height: '80', color: Color.ORANGE }}
          />
          <div>
            <Typography variant="h4" className={title}>
              Соблюдение
            </Typography>{' '}
            <Typography variant="h4" className={title}>
              сроков
            </Typography>
          </div>
          <Typography className={classes.itemMiddleText} variant="body1">
            Современное оборудование обеспечивает высокую скорость работы,
            поэтому заказы сдаем в срок.
          </Typography>
        </CardContent>
      </Card>
      <Card {...props} classes={restClasses}>
        <CardContent className={classes.item}>
          <Icon
            variant="cardItem3"
            style={{ width: '74', height: '80', color: Color.ORANGE }}
          />
          <div>
            <Typography variant="h4">Работаем</Typography>{' '}
            <Typography variant="h4">подоговору</Typography>
          </div>
          <Typography variant="body1">
            Предоставляем полный пакет документов, оплата - наличный и
            безналичный расчет.
          </Typography>
        </CardContent>
      </Card>
    </div>
  )
}
