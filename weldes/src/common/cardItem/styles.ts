import { createStyles } from '@material-ui/core'
import { Color, fontFamily } from '@app/theme'
import makeStyles from '@material-ui/core/styles/makeStyles'
export const useCustomClasses = makeStyles(
  createStyles({
    root: {
      fontFamily,
      width: 384,
      height: 392,
      display: 'flex',
      borderRadius: '0px'
    },
    conteiners: {
      display: 'flex',
      width: 1152,
      height: 392,
      position: 'absolute',
      top: 660
    },
    item: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'start',
      margin: 40
    },
    itemMiddle: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'start',
      margin: 40,
      backgroundColor: Color.BLUE
    },
    itemMiddleText: {
      color: Color.WHITE
    },
    title: {
      fontFamily: fontFamily,
      color: Color.WHITE
    },
    itemMiddleBg: {
      backgroundColor: Color.BLUE
    }
  })
)

export const useCardClasses = makeStyles(
  createStyles({
    root: {
      fontFamily,
      width: 384,
      height: 392,
      display: 'flex',
      borderRadius: '0px'
    }
  })
)
