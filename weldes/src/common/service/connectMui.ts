import withStyles from '@material-ui/core/styles/withStyles'
import { createStyles } from '@material-ui/core'
import { Color, fontFamily } from '@app/theme'
const styles = theme =>
  createStyles({
    root: {
      display: 'flex',
      // flexDirection: 'row',
      alignItems: 'center',
      position: 'relative',
      // backgroundColor: Color.WHITE_LIGHT,
      width: 930,
      height: 450
    },
    reverse: {
      flexDirection: 'row-reverse',
      backgroundColor: Color.TRANSPARENT
    },
    row: {
      flexDirection: 'row',
      backgroundColor: Color.WHITE_LIGHT
    },
    image: {
      width: '448px',
      height: 316
    },

    title: {
      fontFamily: fontFamily,
      color: Color.BLACK,
      fontWeight: 600,
      fontSize: 24,
      lineHeight: '32px',
      padding: '20px 0'
    },
    li: {
      padding: '8px 0'
    }
  })

export default withStyles(styles, { withTheme: true })
