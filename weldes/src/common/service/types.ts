export interface MediaCardProps {
  img: string
  title?: string
  description: string
  list?: string[]
  reverse?: boolean
}
