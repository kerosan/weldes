import { Typography, UL } from '@app/common/typography'
import React from 'react'
import { compose } from 'recompose'
import connectMui from './connectMui'
import { MediaCardProps } from '@app/common/service/types'
import { WithStyles } from '@material-ui/core'
import { Icon } from '@common/icon/icon'
import { Color } from '@app/theme'
import Grid from '@material-ui/core/Grid'

const Service: React.FC<MediaCardProps & WithStyles> = props => {
  const { classes, reverse, ...restProps } = props
  console.log(reverse, classes.reverse)
  const { list, title, image, ...restClasses } = props.classes
  return (
    <Grid
      container
      className={reverse ? classes.reverse : classes.row}
      classes={restClasses}
      {...restProps}
      direction={reverse ? 'row-reverse' : 'row'}
    >
      <Grid item xs={6}>
        <img className={image} src={props.img} alt="service" />
      </Grid>
      <Grid item xs={6}>
        <Typography className={title} variant="h4">
          {props.title}
        </Typography>
        <Typography variant="body1">{props.description}</Typography>
        <UL>
          {props.list &&
            props.list.map((item, index) => (
              <li key={index} className={classes.li}>
                <Icon
                  style={{ padding: '0 16px', width: '16px' }}
                  variant={'check'}
                  fill={Color.ORANGE}
                />
                {item}
              </li>
            ))}
        </UL>
      </Grid>
    </Grid>
  )
}

export default compose<WithStyles, MediaCardProps>(connectMui)(Service)
