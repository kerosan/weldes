import * as React from 'react'
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps
} from 'react-router-dom'
import { Link as LinkMui } from '@material-ui/core'
import { useStyles } from '@common/link/styles'

type Omit<T, K> = Pick<T, Exclude<keyof T, K>>
const CollisionLink = React.forwardRef<
  HTMLAnchorElement,
  Omit<RouterLinkProps, 'innerRef' | 'to'>
>((props, ref) => <RouterLink innerRef={ref as any} to="/" {...props} />)

export const Link = props => {
  const classes = useStyles()
  return (
    <LinkMui
      underline="hover"
      classes={classes}
      component={props.component || (props.to && CollisionLink)}
      {...props}
    >
      {props.children}
    </LinkMui>
  )
}
