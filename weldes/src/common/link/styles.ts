import makeStyles from '@material-ui/core/styles/makeStyles'
import createStyles from '@material-ui/core/styles/createStyles'
import { Color } from '@app/theme'

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      cursor: 'pointer',
      fontSize: 15,
      lineHeight: '17px',
      color: Color.ORANGE,
      '&:visited': {
        color: Color.BLUE
      }
    }
  })
)
