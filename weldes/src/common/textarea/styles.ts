import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'
import makeStyles from '@material-ui/core/styles/makeStyles'

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      borderColor: Color.WHITE_DARK,
      // border: '1px solid ',
      color: Color.GREY,
      minWidth: 330,
      minHeight: 39
    },
    focused: {
      color: Color.ORANGE,
      borderColor: Color.DISABLED_TEXT
    },
    error: {
      // border: '1px solid',
      borderColor: Color.RED
    },
    disabled: {
      borderColor: Color.WHITE_DARK,
      opacity: 0.5
    }
  })
)
