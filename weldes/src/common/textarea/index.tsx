import React from 'react'
import { useStyles } from './styles'
// import InputBase from '@material-ui/core/InputBase'
import { TextField } from '@material-ui/core'

export const Textarea = props => {
  const classes = useStyles()
  return (
    <TextField
      classes={classes}
      margin="dense"
      variant="outlined"
      multiline
      rows="4"
      {...props}
    />
  )
}
