import { createStyles } from '@material-ui/core'
import { Color } from '@app/theme'
import makeStyles from '@material-ui/core/styles/makeStyles'
export const useCustomClasses = makeStyles(
  createStyles({
    root: {
      borderRadius: '0px',
      margin: 16,
      width: 352,
      // maxHeight: 528,
      '&:hover': {
        margin: 12,
        padding: 4,
        backgroundColor: Color.ORANGE,
        boxShadow:
          '0px 12px 17px rgba(0, 0, 0, 0.14), 0px 5px 22px rgba(0, 0, 0, 0.12), 0px 7px 8px rgba(0, 0, 0, 0.2)'
      },
      '&:hover $title': {
        color: Color.ORANGE
      }
    },
    image: {
      backgroundSize: 'auto',
      height: 208,
      width: '100%',
      // position: 'relative'
      '&:hover': {
        backgroundColor: Color.ORANGE,
        borderRadius: '2px',
        borderColor: Color.ORANGE
      }
    }
  })
)

// export default withStyles(styles, { withTheme: true })
