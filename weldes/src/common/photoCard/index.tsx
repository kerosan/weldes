import CardMedia from '@material-ui/core/CardMedia'
import Card from '@material-ui/core/Card'
import React from 'react'
import { useCustomClasses } from './style'
import { PhotoCardProps } from '@app/common/photoCard/types'

export const PhotoCard: React.FC<PhotoCardProps> = props => {
  const classes = useCustomClasses()
  const { image, ...restClasses } = classes
  return (
    <Card {...props} classes={restClasses}>
      <CardMedia className={image} image={props.img}></CardMedia>
    </Card>
  )
}
