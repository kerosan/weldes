export interface MediaCardProps {
  img: string
  date: string
  title: string
  description: string
  url: string
}
