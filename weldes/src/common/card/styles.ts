import { createStyles } from '@material-ui/core'
import { Color, fontFamily } from '@app/theme'
import makeStyles from '@material-ui/core/styles/makeStyles'
export const useCustomClasses = makeStyles(
  createStyles({
    root: {
      position: 'relative',
      margin: '16px 0px',
      fontFamily,

      '& img': {
        width: '100%',
        height: 208
      },
      width: 352,
      maxHeight: 528,
      '&:hover': {
        backgroundColor: Color.WHITE,
        boxShadow:
          '0px 12px 17px rgba(0, 0, 0, 0.14), 0px 5px 22px rgba(0, 0, 0, 0.12), 0px 7px 8px rgba(0, 0, 0, 0.2)'
      },
      '&:hover $title': {
        color: Color.ORANGE
      }
    },
    container: {
      width: 1120,
      margin: '56px 0px 96px 0px'
    },
    image: {
      height: 208,
      position: 'relative'
    },
    date: {
      fontSize: 14,
      lineHeight: '20px',
      color: Color.WHITE,
      fontFamily,
      position: 'absolute',
      left: 24,
      bottom: 19,
      userSelect: 'none'
    },
    title: {
      fontFamily: fontFamily,
      color: Color.PRIMARY_TEXT,
      fontWeight: 600,
      fontSize: 20,
      lineHeight: '32px'
    }
  })
)

// export default withStyles(styles, { withTheme: true })
