import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import { Typography } from '@app/common/typography'
import CardActions from '@material-ui/core/CardActions'
import { Button } from '@app/common/button'
import Card from '@material-ui/core/Card'
import React from 'react'
import { useCustomClasses } from './styles'
import { MediaCardProps } from '@app/common/card/types'

export const MediaCard: React.FC<MediaCardProps> = props => {
  const classes = useCustomClasses()
  const { date, title, image, ...restClasses } = classes
  return (
    <Card {...props} classes={restClasses}>
      <CardMedia className={image} image={props.img}>
        <span className={date}>{props.date}</span>
      </CardMedia>
      <CardContent>
        <Typography variant="h4" className={title}>
          {props.title}
        </Typography>
        <Typography variant="body1">{props.description}</Typography>
      </CardContent>
      <CardActions>
        <Button href={props.url}>ПОДРОБНЕЕ</Button>
      </CardActions>
    </Card>
  )
}
