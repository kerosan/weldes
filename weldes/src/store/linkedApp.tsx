import * as React from 'react'
import { Provider } from 'react-redux'
// import { Snackbar } from 'react-redux-snackbar'

import AppRouter from '@app/router/index'
// import { StyledSnack } from '@app/components/common/styled'
import configureStore from '@app/store/index'
// import { ROUTES } from '@app/constants'
import { history as browserHistory } from '@app/router/history'
import { useGlobal } from '@app/theme'

export const store = configureStore()

const LinkedApp: React.FunctionComponent<{ history?: any }> = () => {
  useGlobal({})
  return (
    <Provider store={store}>
      <>
        <AppRouter history={browserHistory} />
        {/* <StyledSnack> */}
        {/* <Snackbar /> */}
        {/* </StyledSnack> */}
      </>
    </Provider>
  )
}
if (module.hot) {
  module.hot.accept('@app/store', () => {
    const nextRootReducer = require('@app/store').default
    store.replaceReducer(nextRootReducer)
  })
}

export default LinkedApp
