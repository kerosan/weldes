import { createStore, compose, applyMiddleware } from 'redux'
import createSagaMiddleWare from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'
import { Store } from 'react-redux'

import { history } from '@app/router/history'
import { IRootReducer } from './interface'
import { reducer } from './reducer'
import { saga } from './saga'

let composeEnhancers: any = null
if (
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
  // @ts-ignore
  typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function'
) {
  // @ts-ignore
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    trace: true,
    traceLimit: 25
  })
} else {
  composeEnhancers = compose
}

export default function configureStore(initialState?: IRootReducer) {
  const sagaMiddleWare = createSagaMiddleWare()
  const store: Store<IRootReducer> = createStore<IRootReducer, any, any, any>(
    reducer,
    initialState,
    composeEnhancers(
      compose(
        applyMiddleware(sagaMiddleWare),
        applyMiddleware(routerMiddleware(history))
      )
    )
  )
  sagaMiddleWare.run(saga) // store.dispatch
  if (module.hot) {
    module.hot.accept('@app/store', () => {
      store.replaceReducer(require('@app/store').reducer)
    })
  }

  return store
}
