import { combineReducers } from 'redux'

import { connectRouter } from 'connected-react-router'
import { history } from '@app/router/history'
import { IRootReducer } from './interface'

const router = connectRouter(history)

export const reducer = combineReducers<IRootReducer>({
  router
})
