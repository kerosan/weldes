import makeStyles from '@material-ui/core/styles/makeStyles'
import { createStyles } from '@material-ui/core'

export const Color = {
  WHITE: '#FFFFFF',
  WHITE_LIGHT: '#F5F5F5',
  WHITE_DARK: '#DEDEDE',
  DIVIDER: '#DEDEDE',
  GREY: '#666666',
  SECONDARY_TEXT: '#666666',
  DISABLED_TEXT: '#9E9E9E',
  GREY_HOVER: '#f8f8f8',
  GREY_ACTIVE: '#efefef',
  RED: '#B00020',
  ERROR: '#B00020',
  ORANGE: '#FA983E',
  ORANGE_DARK: '#EE7738',
  BLUE: '#1E3C56',
  PRIMARY_TEXT: '#212121',
  BLACK: '#212121',
  TRANSPARENT: 'transparent'
}
export const fontFamily = '"Open Sans", sans-serif'

export const useGlobal = makeStyles(
  createStyles({
    '@global': {
      body: {
        margin: 0,
        fontFamily
      }
    }
  })
)
