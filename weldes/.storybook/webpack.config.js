// you can use this file to add your custom webpack plugins, loaders and anything you like.
// This is just the basic way to add additional webpack configurations.
// For more information refer the docs: https://storybook.js.org/configurations/custom-webpack-config
const path = require("path");
// IMPORTANT
// When you add this file, we won't add the default configurations which is similar
// to "React Create App". This only has babel loader to load JavaScript.

module.exports = ({ config, mode }) => {
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    loader: require.resolve('babel-loader'),
    options: {
      presets: [['react-app', { flow: false, typescript: true }]],
    },
  });
  config.resolve.extensions.push('.ts', '.tsx');
  config.resolve.alias = {
    "@app": path.resolve(__dirname, "../src"),
    "@assets": path.resolve(__dirname, "../src/assets"),
    "@common": path.resolve(__dirname, "../src/common"),
    "@components": path.resolve(__dirname, "../src/components"),
    "@containers": path.resolve(__dirname, "../src/containers"),
    "@store": path.resolve(__dirname, "../src/store")
  };
  return config;
};